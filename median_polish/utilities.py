from typing import cast
from numpy import ndarray, median


def med_abs_dev(data: ndarray) -> float:
    """Median absolute deviation.
    MAD = median(|X_i - median(X)|)
    """
    return cast(float, median(abs(data - median(data))))
