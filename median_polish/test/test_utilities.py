import numpy as np
from pytest import fixture
from median_polish.utilities import med_abs_dev

@fixture
def simple_table():
    return np.array([[25.3, 25.3, 18.2, 18.3, 16.3],
                     [32.1, 29.0, 18.8, 24.3, 19.0],
                     [38.8, 31.0, 19.3, 15.7, 16.8],
                     [25.4, 21.1, 20.3, 24.0, 17.5]])

def test_median_absolute_deviation(simple_table):
    result = med_abs_dev(simple_table)
    assert result == 3.75
