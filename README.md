# Python Median Polish
_Keji Li_

Median polish a non-parametric exploratory statistical method proposed by Tukey.

[![pipeline status](https://gitlab.com/Palpatineli/median_polish/badges/master/pipeline.svg)](https://gitlab.com/Palpatineli/median_polish/-/commits/master)
[![coverage report](https://gitlab.com/Palpatineli/median_polish/badges/master/coverage.svg)](https://gitlab.com/Palpatineli/median_polish/-/commits/master)

It assumes an additive model, with constant random marginal effects (e.g.: random (categorical) row and column effect in a two-way table).
This repository contains a python/numpy implementation of this method used on 2d numpy.ndarray data.

This package can be found on pypi as median_polish.
